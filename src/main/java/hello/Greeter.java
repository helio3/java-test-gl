package hello;

public class Greeter {

  public String sayHello() {
    return "Hello world!";
  }

  private String sayNay(String toWho) {
    return "Nay!";
  }

  public void method1() {
    method2("String", null);
  }

  public void method2(String param1, String param2) {
    String var1 = param1.trim();
    param2 = param2.trim();
    System.out.println(var1);
    System.out.println(param2);
  }

  public void method3(TestObject testobject) {
    String var1 = testobject.get("test");
    System.out.println(var1);
    if (testobject == null) {
      return;
    }
  }
}
