package hello;

import com.google.common.base.Optional;

import java.util.concurrent.atomic.AtomicLong;

import java.sql.*;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.Map;


public class Dummy{

    public Connection generateConnection(){
        //TODO: implement a connection method
        return null;
    }
    
    public void TestSQLInjection(Optional<Integer> userId, Optional<String> password){
        Connection connection = generateConnection();
        try{
            String query = "SELECT * FROM users WHERE userid ='"+ userId.or(0) + "'" + " AND password='" + password.or("YAY") + "'";
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);
        }catch(Exception ex){
            ex.printStackTrace();
        }
     }
}